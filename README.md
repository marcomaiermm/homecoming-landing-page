# Landing Page Projekt Homecoming

Dieses Repo enthält die Landing Page für die App hausbaumanager.

## Verwendeter Stack

    - [Vite](https://vite.io/)
    - [Tailwind](https://tailwindcss.com/)
    - [Alpinejs](https://alpinejs.org/)
    - [TypeScript](https://www.typescriptlang.org/)

## Installation

npm:

```bash
npm install
npm run dev
```

yarn:
```bash
yarn install
yarn dev
```

Go to http://localhost:3000/