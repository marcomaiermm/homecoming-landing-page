export interface ICounter {
  id?: number;
  label: string;
  target: number;
  value?: number;
  unit: string;
}

export interface IEmail {
  email: string;
}