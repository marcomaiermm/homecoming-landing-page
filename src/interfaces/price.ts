export interface IPrice {
  label: string;
  cost_per_month: number;
  features: string[];
  cost?: number;
  monthly?: boolean;
}
