import { ICounter, IEmail } from "./interfaces/data";
import "./tailwind.css";
import Alpine from "alpinejs";
import intersect from "@alpinejs/intersect";
import pricing from "./pricing.json";
import counterData from "./data.json";
import { IPrice } from "./interfaces/price";

const app = () => ({
  monthly: false,
  modal: false,
  counterData: [] as ICounter[],
  counterInterval: 1000,
  prices: [] as IPrice[],
  email: "",
  api: "http://localhost:3000",
  generatePrices(): IPrice[] {
    // Function to generate prices

    // Shallow Copy of pricing data
    const prices = pricing.map((price: IPrice) => ({ ...price }));

    prices.forEach((price: IPrice) => {
      price.cost = price.cost_per_month * (this.monthly ? 1 : 10);
      price.monthly = this.monthly;
    });

    return prices;
  },
  setMonthly(): void {
    this.monthly = true;
    this.prices = this.generatePrices();
  },
  setAnnual(): void {
    this.monthly = false;
    this.prices = this.generatePrices();
  },
  getCurrentYear(): number {
    return new Date().getFullYear();
  },
  formatDigits(num: number): string {
    return (Math.round(num * 100) / 100).toFixed(2);
  },
  async submit(): Promise<void> {
    const payload = {
      email: this.email
    } as IEmail

    const post = await fetch(this.api + "/email", {
      method: "post",
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(payload)
    })
    const data = post.status;
    if (data === 200) {
      this.modal = true;
      this.email = "";
    }

  },
  async countDownTimer(data: ICounter): Promise<number> {
    const countdown = setInterval(() => {
      if (data.value) data.value += 1;
      if (data.value && data.value >= data.target) clearInterval(countdown);
    }, this.counterInterval);
    return countdown;
  },
  startTimer(data: ICounter): void {
    const id = data.id ?? 0;
    this.countDownTimer(this.counterData[id]);
  },
  async init(): Promise<void> {
    this.prices = this.generatePrices();
    this.counterData = counterData.map((counter: ICounter, index: number) => ({
      ...counter,
      id: index,
      value: 0,
    }));
  },
});

window.Alpine = Alpine;
Alpine.plugin(intersect);
Alpine.data("app", app);
Alpine.start();
